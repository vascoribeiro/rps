package main;

import game.Game;
import game.IGame;
import game.Score;

public class Main {
	public static void main(String[] args) {
    
		 IGame game = new Game();
		 Score score = game.play();
		 game.write(score);
	}
}
