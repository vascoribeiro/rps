package enumerator;

import java.util.EnumMap;
import java.util.Map;

public enum HandDraw {
	ROCK, PAPER, SCISSORS;
	
    private static final Map<HandDraw, HandDraw> win = new EnumMap<>(HandDraw.class);

    static {
        win.put(ROCK, SCISSORS);
        win.put(PAPER, ROCK);
        win.put(SCISSORS, PAPER);
    }
    
    public static HandDraw wins(HandDraw m) {
        return win.get(m);
    }
}
