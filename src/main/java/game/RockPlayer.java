package game;

import enumerator.HandDraw;


class RockPlayer implements IPlayer{
	
	@Override
	public HandDraw getNextHandDraw() {
		return HandDraw.ROCK;
	}
}
