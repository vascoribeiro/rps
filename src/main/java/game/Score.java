package game;

public class Score {
	private Integer rockPlayer;
	private Integer randomPlayer;
	private Integer tie;
	
	Score(Integer randomPlayer, Integer rockPlayer, Integer tie){
		this.randomPlayer = randomPlayer;
		this.rockPlayer = rockPlayer;
		this.tie = tie;
	}

	public Integer getRockPlayer() {
		return rockPlayer;
	}
	
	public void addOneRockPlayer(){
		this.rockPlayer = this.rockPlayer + 1;
	}
	
	public Integer getRandomPlayer() {
		return randomPlayer;
	}
	
	public void addOneRandomPlayer(){
		this.randomPlayer = this.randomPlayer + 1;
	}

	public Integer getTie() {
		return tie;
	}
	
	public void addOneTie(){
		this.tie = this.tie + 1;
	}
	
}
