package game;

import java.util.Random;

import enumerator.HandDraw;

class RandomPlayer implements IPlayer {


    private static final HandDraw[] handDraw   = HandDraw.values();

    /**
     * The random number generator used; created once and then cached
     */
    private final Random random;

    RandomPlayer() {
        random = new Random();
    }

    @Override
    public HandDraw getNextHandDraw() {
        return handDraw[random.nextInt(handDraw.length)];
    }
}