package game;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsoleWriter implements IWriter{
	
	private static Logger logger = LoggerFactory.getLogger(ConsoleWriter.class);
	
	@Override
	public void putScore(Score score) {
		logger.info(" Rock player won: " + score.getRockPlayer() +" - Random player won: " + score.getRandomPlayer()+ " - Tie: " + score.getTie()+" -");
	}

	

}
