package game;

public interface IGame {
	public Score play();
	public void write(Score score);
}
