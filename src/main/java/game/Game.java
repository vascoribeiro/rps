package game;

import enumerator.HandDraw;

public class Game implements IGame{

	private static final int ROUNDS = 100;
	private static final Integer zero = 0;
	private IPlayer rockPlayer;
	private IPlayer randomPlayer;
	
	
	public Game() {
		 rockPlayer = new RockPlayer();
	     randomPlayer = new RandomPlayer();
    }
	
	@Override
	public void write(Score score){
		IWriter writer = new ConsoleWriter();
		writer.putScore(score);
	}
	
	@Override
    public Score play(){
    	Score score = new Score(zero,zero,zero);
    	for(int i = 1; i<= ROUNDS; i++ ){
    		score = evaluate(rockPlayer.getNextHandDraw(),randomPlayer.getNextHandDraw(),score);
    	}
    	return score;
    }
    
    protected Score evaluate(HandDraw rock, HandDraw random, Score score){
        	
    	if (HandDraw.wins(rock).equals(random)) {
    		score.addOneRockPlayer();
    	}
    	else if (HandDraw.wins(random).equals(rock)) {
    		score.addOneRandomPlayer();
    		
        }
    	else{
    		score.addOneTie();
    	}
    	
    	return score;
    }
    
}
