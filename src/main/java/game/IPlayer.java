package game;

import enumerator.HandDraw;



interface IPlayer {
	HandDraw getNextHandDraw();
}
