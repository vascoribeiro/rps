package game;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import enumerator.HandDraw;

public class RandomPlayerTest {
	private RandomPlayer randomPlayerFirst;
	private Proportion tenProportion, hundredProportion, 
			tenThousandProportion, millionProportion;
	
	@Before
	public void setUp() throws Exception {
		randomPlayerFirst = new RandomPlayer();
		tenProportion = null;
		hundredProportion = null; 
		tenThousandProportion = null;
		millionProportion = null;
	}
	
	@Test
	public void testLawLargeNumbers(){
		
		int rock = 0, paper = 0, scissors = 0;
		double average = (double)1/(double)3;

		for(int i = 0; i <= 1000000; i++){
			if(randomPlayerFirst.getNextHandDraw().equals(HandDraw.ROCK))
				rock++;
			if(randomPlayerFirst.getNextHandDraw().equals(HandDraw.PAPER))
				paper++;
			if(randomPlayerFirst.getNextHandDraw().equals(HandDraw.SCISSORS))
				scissors++;	
			if(i == 10){
				tenProportion = new Proportion(rock, paper, scissors, 10);
			}
			if(i == 100){
				hundredProportion = new Proportion(rock, paper, scissors, 100);
			}
			if(i == 10000){
				tenThousandProportion = new Proportion(rock, paper, scissors, 10000);
			}
			if(i == 1000000){
				millionProportion = new Proportion(rock, paper, scissors, 1000000);
			}
		}
		
		assertTrue(Math.abs(tenProportion.getRockProportion() - average) > Math.abs(tenThousandProportion.getRockProportion() - average));
		assertTrue(Math.abs(hundredProportion.getRockProportion() - average) > Math.abs(millionProportion.getRockProportion() - average));
		
		assertTrue(Math.abs(tenProportion.getPaperProportion() - average) > Math.abs(tenThousandProportion.getPaperProportion() - average));
		assertTrue(Math.abs(hundredProportion.getPaperProportion() - average) > Math.abs(millionProportion.getPaperProportion() - average));
		
		assertTrue(Math.abs(tenProportion.getScissorsProportion() - average) > Math.abs(tenThousandProportion.getScissorsProportion() - average));
		assertTrue(Math.abs(hundredProportion.getScissorsProportion() - average) > Math.abs(millionProportion.getScissorsProportion() - average));

	}

	@After 
	public void tearDown() throws Exception {
		randomPlayerFirst = null;
		tenProportion = null;
		hundredProportion = null; 
		tenThousandProportion = null;
		millionProportion = null;
	}
	
	private class Proportion {
		private double rock, paper, scissors, total;
	
	
		protected Proportion(int rock, int paper, int scissors, int total) {
			this.rock = rock;
			this.paper = paper;
			this.scissors = scissors;
			this.total = total;
		}

		protected double getRockProportion() {
			return this.rock / this.total;
		}

		protected double getPaperProportion() {
			return this.paper / this.total;
		}

		protected double getScissorsProportion() {
			return this.scissors / this.total ;
		}
    
	}
}
