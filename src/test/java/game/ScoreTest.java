package game;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ScoreTest {
	private Score score;
	private static final Integer ten = 10, twenty = 20, thirty = 30;
	private static final Integer zero = 0, one = 1;
	
	@Test
	public void testConstructorOfScore(){
		score = new Score(ten, twenty, thirty);
		assertEquals(ten, score.getRandomPlayer());
		assertEquals(twenty, score.getRockPlayer());
		assertEquals(thirty, score.getTie());
	}
	
	@Test
	public void testAddOneRockPlayer(){
		score = new Score(zero, zero, zero);
		score.addOneRockPlayer();
		assertEquals(one, score.getRockPlayer());
		assertEquals(zero, score.getRandomPlayer());
		assertEquals(zero, score.getTie());
	}
	
	@Test
	public void testAddOneRandomPlayer(){
		score = new Score(zero, zero, zero);
		score.addOneRandomPlayer();
		assertEquals(one, score.getRandomPlayer());
		assertEquals(zero, score.getRockPlayer());
		assertEquals(zero, score.getTie());
	}
	
	@Test
	public void testAddOneTie(){
		score = new Score(zero, zero, zero);
		score.addOneTie();
		assertEquals(one, score.getTie());
		assertEquals(zero, score.getRockPlayer());
		assertEquals(zero, score.getRandomPlayer());
	}
}
