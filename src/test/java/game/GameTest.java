package game;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.mockito.Mockito;

import enumerator.HandDraw;


public class GameTest {
	private Game game;

	private static final Integer one = 1, zero = 0;
	
	@Test
	public void testPlay(){
		Integer hundred = 100, total;
		
		game = new Game();
		Score score = game.play();
		
		total = score.getRandomPlayer() + score.getRockPlayer() + score.getTie();

		assertEquals(hundred, total);
	}
	
	@Test 
	public void testWrite(){
		game = new Game();
		Score score = new Score(0,0,0);
		game.write(score);
		
		IWriter writer = Mockito.mock(ConsoleWriter.class);;
		
		
	}
	
	
	@Test
	public void testEvaluatePaperRock(){
		game = new Game();
		Score score = new Score(zero,zero,zero);
		score = game.evaluate(HandDraw.PAPER, HandDraw.ROCK, score);
		assertEquals(one, score.getRockPlayer());
		assertEquals(zero, score.getRandomPlayer());
		assertEquals(zero, score.getTie());
	}
	
	@Test
	public void testEvaluateRockPaper(){
		game = new Game();
		Score score = new Score(zero,zero,zero);
		score = game.evaluate(HandDraw.ROCK,HandDraw.PAPER, score);
		assertEquals(one, score.getRandomPlayer());	
		assertEquals(zero, score.getRockPlayer());
		assertEquals(zero, score.getTie());
	}
	
	@Test
	public void testEvaluateRockScissors(){
		game = new Game();
		Score score = new Score(zero,zero,zero);
		score = game.evaluate(HandDraw.ROCK,HandDraw.SCISSORS, score);
		assertEquals(one, score.getRockPlayer());
		assertEquals(zero, score.getRandomPlayer());
		assertEquals(zero, score.getTie());
	}
	
	@Test
	public void testEvaluateScissorsRock(){
		game = new Game();
		Score score = new Score(zero,zero,zero);
		score = game.evaluate(HandDraw.SCISSORS,HandDraw.ROCK, score);
		assertEquals(one, score.getRandomPlayer());
		assertEquals(zero, score.getRockPlayer());
		assertEquals(zero, score.getTie());
	}
	
	@Test
	public void testEvaluateScissorsPaper(){
		game = new Game();
		Score score = new Score(zero,zero,zero);
		score = game.evaluate(HandDraw.SCISSORS,HandDraw.PAPER, score);
		assertEquals(one, score.getRockPlayer());
		assertEquals(zero, score.getRandomPlayer());
		assertEquals(zero, score.getTie());
	}
	
	@Test
	public void testEvaluatePaperScissors(){
		game = new Game();
		Score score = new Score(zero,zero,zero);
		score = game.evaluate(HandDraw.PAPER,HandDraw.SCISSORS, score);
		assertEquals(one, score.getRandomPlayer());
		assertEquals(zero, score.getRockPlayer());
		assertEquals(zero, score.getTie());
	}
	
	@Test
	public void testEvaluatePaperPaper(){
		game = new Game();
		Score score = new Score(zero,zero,zero);
		score = game.evaluate(HandDraw.PAPER,HandDraw.PAPER, score);
		assertEquals(one, score.getTie());
		assertEquals(zero, score.getRandomPlayer());
		assertEquals(zero, score.getRockPlayer());
	}
	
	@Test
	public void testEvaluateRockRock(){
		game = new Game();
		Score score = new Score(zero,zero,zero);
		score = game.evaluate(HandDraw.ROCK,HandDraw.ROCK, score);
		assertEquals(one, score.getTie());
		assertEquals(zero, score.getRandomPlayer());
		assertEquals(zero, score.getRockPlayer());
	}
	
	@Test
	public void testEvaluateScissorsScissors(){
		game = new Game();
		Score score = new Score(zero,zero,zero);
		score = game.evaluate(HandDraw.SCISSORS,HandDraw.SCISSORS, score);
		assertEquals(one, score.getTie());
		assertEquals(zero, score.getRandomPlayer());
		assertEquals(zero, score.getRockPlayer());
	}
	
}