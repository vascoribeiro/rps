package game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import enumerator.HandDraw;

public class RockPlayerTest {
	private IPlayer rockPlayer;
	
	@Before
	public void setUp() throws Exception {
		rockPlayer = new RockPlayer();
	}
	
	@Test
	public void isRock(){
		assertEquals(HandDraw.ROCK, rockPlayer.getNextHandDraw());
	}
	
	@Test
	public void isNotRock(){
		assertNotEquals(HandDraw.PAPER, rockPlayer.getNextHandDraw());
		assertNotEquals(HandDraw.SCISSORS, rockPlayer.getNextHandDraw());
	}
	
	@After 
	public void tearDown() throws Exception {
		rockPlayer = null;
	}

}


