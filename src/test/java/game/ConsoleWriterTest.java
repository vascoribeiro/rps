package game;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.verify;

import org.slf4j.LoggerFactory;

import static org.hamcrest.CoreMatchers.is;

import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ConsoleWriterTest {

	private IWriter writer;
	
	@Mock
	private Appender mockAppender;

	@Captor
	private ArgumentCaptor<LoggingEvent> captorLoggingEvent;

	@Before
	public void setup() {
		final Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
		logger.addAppender(mockAppender);
	}

	@After
	public void teardown() {
		final Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
		logger.detachAppender(mockAppender);
	}

	@Test
	public void testPutScore() {
	        
		writer = new ConsoleWriter();;
	    Score score = new Score(0,0,0);
	    writer.putScore(score);

	    verify(mockAppender).doAppend(captorLoggingEvent.capture());
	      
	    final LoggingEvent loggingEvent = captorLoggingEvent.getValue();
	      
	    assertThat(loggingEvent.getLevel(), is(Level.INFO));
	    
	    assertThat(loggingEvent.getFormattedMessage(),
	                is("Rock player won: " + score.getRockPlayer() +" Random player won: " + score.getRandomPlayer()+ " Tie: " + score.getTie()));
	}
	
}
