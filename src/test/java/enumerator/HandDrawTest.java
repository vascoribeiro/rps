package enumerator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;




public class HandDrawTest {

	@Test
	public void testWinsPaper() throws Exception {
		assertEquals(HandDraw.wins(HandDraw.PAPER), HandDraw.ROCK);
	}
	
	@Test
	public void testWinsRock() throws Exception {
		assertEquals(HandDraw.wins(HandDraw.ROCK), HandDraw.SCISSORS);
	}

	@Test
	public void testWinsScissors() throws Exception{
		assertEquals(HandDraw.wins(HandDraw.SCISSORS), HandDraw.PAPER);
	}
}
